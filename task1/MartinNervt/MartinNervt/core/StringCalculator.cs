﻿using System;
using System.Collections.Generic;

namespace MartinNervt.core
{
    public class StringCalculator
    {
        public int Add(String numbers)
        {

            NumberParser numberParser = new NumberParser();
            List<int> parsedNumbers = numberParser.ParseString(numbers);

            int sum = 0;
            for (int i = 0; i < parsedNumbers.Count; i++)
            {
                sum = sum + parsedNumbers[i];
            }
            return sum;
        }
    }
}
