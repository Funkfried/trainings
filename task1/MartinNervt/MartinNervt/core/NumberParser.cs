﻿using System;
using System.Collections.Generic;

namespace MartinNervt.core
{
    public class NumberParser
    {
        private readonly static char[] seperators = new char[] { ',', '\n' };

        public List<int> ParseString(string numbers)
        {
            List<int> parsedNumbers = new List<int>(0);

            if (IsEmpty(numbers))
            {
                parsedNumbers.Add(0);
            }

            else
            {
                ConvertStringToListOfNumbers(numbers, parsedNumbers);
            }
            return parsedNumbers;
        }

        private static void ConvertStringToListOfNumbers(string numbers, List<int> parsedNumbers)
        {
            String[] extractedNumbers = ExtractNumbersFromString(numbers);

            for (int i = 0; i < extractedNumbers.Length; i++)
            {
                parsedNumbers.Add(int.Parse(extractedNumbers[i]));
            }

        }

        private static String[] ExtractNumbersFromString(string numbers)
        {
            return numbers.Split(seperators);
        }

        private static bool IsEmpty(String numbers)
        {
            if (numbers == "")
            {
                return true;
            }
            return false;
        }
    }
}
