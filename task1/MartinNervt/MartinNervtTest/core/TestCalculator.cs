﻿using MartinNervt.core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MartinNervtTest.core
{
    [TestClass]
    public class TestCalculator
    {
        [TestMethod]
        public void EmptyString()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("");

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void OneNumberAsString()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("1");

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void TwoNumbersAsString()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("12,18");

            Assert.AreEqual(30, result);
        }

        [TestMethod]
        public void TenNumbersAsString()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("1,2,3,4,5,6,7,8,9,10");

            Assert.AreEqual(55, result);
        }

        [TestMethod]
        public void NewlineAsSepearator()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("1\n2");

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void NewlineAndCommaAsSepearator()
        {
            StringCalculator tested = new StringCalculator();

            int result = tested.Add("1\n2,3");

            Assert.AreEqual(6, result);
        }
    }
}
