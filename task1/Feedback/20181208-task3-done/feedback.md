# TESTS #
## Names ##
  - look at the picture
  - what is the test doing / checking? Imagine you have 1000 test and one is failing. Would be nice if I can see what i did wrong without deepdiving into the test code
  - best practices: https://dzone.com/articles/7-popular-unit-test-naming
  - my favorite: 
    - EmptyStringIsZero
    - SingleNumberIsParsed
    - TwoNumbersAreAdded
    - ListOfNumbersAreAdded
    - NewLineAsAllowedSeparator
## Structure ##
  - Duplicated initialization (Setup method for unit under test)
## Values ##
  - test values are to complicated for unittests
  - no additional value to check 12 and 18 instead of 1 and 2, but 1 and 2 is for everyone easy to check ;)
  - same for "list of numbers": 1,2,3 is sufficient

# StringCalculator #
  - nice extraction of the numberparser --> i think you remembered it from our earlier dojos :D
  - BUT NumberParser is a must have for this class, therefore it should be initialized in the constructor of the class
  - input for the method should be "string" not "String" because c# supports the primitive version of this
  - what about extracting the calculation logic to an separate class as well. maybe something like Adder taking the list as an input and giving the sum as an output
    - this follows the idea of Open-Close-Principle (see SOLID)

# NumberParser # 
  - hard to understand because variables are leaving the contexts (e.g. parsedNumbers)
  - goes hand in hand with missed levels of abstractions
    - general idea: one method should do one thing and handle only a limited (only known to itself) list of variables
    - bestcase would be if each method could be extracted to a dedicated class
  - try to use early returns as often as possible. Supports easier reading.
    - e.g. IsEmpty --> return List containing 0 only
  - BONUS: have a look at the c# feature "yield return"
  - wrong method name for "private static String[] ExtractNumbersFromString(string numbers)". I do not see numbers extracted (from method definition impossible)
    - name could be valid for "private static int[] ExtractNumbersFromString(string numbers)"
  - primitives are types which are not objects. Therefore you should not treat them as such. 
    - here you are calling a method on a primitve "parsedNumbers.Add(int.Parse(extractedNumbers[i]));"
	- what happens: the compiler is chaning it in the background to an object. this is called AUTOBOXING
  - "==" should always be avoided if you are not 100% sure
    - here you made it on a Object:
		private static bool IsEmpty(String numbers)
        {
            if (numbers == "")
		[...]
		}
  - By the way: do not return booleans in an if statement. return the condition :) 
  - Mostly there are helper methods for "basic things" (i had to google it as well)
	- check: String.IsNullOrEmpty(numbers);